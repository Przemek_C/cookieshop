package com.example;


import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;

public class MyGenerator {
    public static void main(String[] args) {
        Schema schema = new Schema(2, "com.ciaston.przemek.shop.db");
        schema.enableKeepSectionsByDefault();

        addTables(schema);

        try {
            new DaoGenerator().generateAll(schema, "./app/src/main/java");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void addTables(Schema schema) {
        Entity product = schema.addEntity("Products");
        product.addIdProperty().primaryKey().autoincrement();
        product.addStringProperty("name");
        product.addFloatProperty("price");

        Entity client = schema.addEntity("Clients");
        client.addIdProperty().primaryKey().autoincrement();
        client.addStringProperty("Name");
        client.addStringProperty("Surename");
        client.addDateProperty("Birth_date");
        client.addStringProperty("City");
        client.addStringProperty("Street");
        client.addStringProperty("Postal_code");
        client.addIntProperty("Phone");

        Entity cart = schema.addEntity("Cart");
        cart.addIdProperty().primaryKey().autoincrement();
        Property productIdProperty = cart.addLongProperty("productId").getProperty();
        cart.addToOne(cart, productIdProperty);
        cart.addFloatProperty("count");
        cart.addFloatProperty("price");

//        Entity orders = schema.addEntity("Order_detals");
//        orders.addIdProperty().primaryKey().autoincrement();
//        Property clientIdProperty = client.addLongProperty("clientId").getProperty();
//        orders.addToOne(orders, clientIdProperty);


    }
}
